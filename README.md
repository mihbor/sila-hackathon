# SALT - SiLA - AnIML Laboratory Technology

This code has been created by the Future Corner by the SALT team.

Please check out the [Presentation](https://drive.google.com/file/d/11UkCDoMPza-KUV7bueeCv5Hpf5Mg-5_Q/view?usp=sharing)

# Components
## sila-executor
The executor is orchestrating several SiLA Devices in a workflow as follows:
* Find "BarcodeReader" SiLA Device and read sample barcode
* Merge barcode information into an AniML file using the data-aggration-service
* Find "Multidrop" SiLA Device and run sample prep dispense
*  Merge dispenser information into an AniML file using the data-aggration-service
* Find "HPLCDevice" SiLA Device and run sample injection
* Get back chromatogram in AnIML format
* Merge information into an AniML file using the data-aggration-service

## hplc
This is a SiLA Device (virtual HPLC device) simulating a chromatography measurement and returning an chromatogram as an AnIML result.

## data-aggration-service
The aggregation service aggregates information from the different AnIML files that were produced in the above workflow into a final single result file and 
uploads it to the [Seahorse Cloud](https://seahorse.cloud/).
Use username `pistoia` and password `hackathOn`.

# SiLA Standard
SiLA’s mission is to establish international standards which create open connectivity in lab automation. SiLA’s vision is to create interoperability, flexibility and resource optimization for laboratory instruments integration and software services based on standardized communication protocols and content specifications. SiLA promotes open standards to allow integration and exchange of intelligent systems in a cost effective way.

The SiLA 2 specification is a multi part specification and the work-in-progress
documents can be accessed on google drive:

* [Part (A) - Overview, Concepts and Core Specification](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit)
* [Part (B) - Mapping Specification](https://docs.google.com/document/d/1-shgqdYW4sgYIb5vWZ8xTwCUO_bqE13oBEX8rYY_SJA/edit)
* [Part (C) - Features Index](https://docs.google.com/document/d/1J9gypD6HofLQZ8cPgLWljRuO0V8l5dS22TWQxFy4bhY/edit)

For more information, visit our [website](http://sila-standard.com/).

In case of general questions, contact either Max ([max@unitelabs.ch](mailto:max@unitelabs.ch)) or Daniel ([daniel.juchli@sila-standard.org](mailto:daniel.juchli@sila-standard.org)).

# AnIML Standard
The Analytical Information Markup Language (AnIML) is the emerging ASTM XML standard for analytical chemistry data. It is currently in pre-release form It is a combination of:

* A highly flexible core schema that defines XML tagging for any kind of analytical information;
* A set of technique definition documents. These XML files, one per analytical technique, apply tight constraints to the flexible core and in turn are defined by the Technique Schema;
* Extensions to Technique Definitions are possible to accommodate vendor- and institution-specific data fields.

For more information, visit our [website](http://animl.org/).


# License
This code is licensed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License)