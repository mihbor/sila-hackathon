package hplc;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.apache.logging.log4j.core.util.IOUtils;
import sila_features.SiLAServiceDevice;
import sila_tools.bridge.SiLABridge;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.NetworkInterface;
import java.time.Year;
import java.util.*;

import static sila_library.utils.SiLAErrors.generateValidationError;
import static sila_library.utils.Utils.blockUntilStop;

/**
 * Example SiLA Device using mandatory SiLA Capabilities
 */
public class HPLCDevice {
    public final static String DEVICE_NAME = "HPLCDevice";
    private final static int BRIDGE_PORT = 50051; // Default
    private final static int DEVICE_PORT = 50052; // Default
    private final SiLABridge bridge;
    private Server server;

    public HPLCDevice(@Nonnull String interfaceName) {
        this.bridge = new SiLABridge(interfaceName);
    }

    private void start(int proxyPort, int devicePort) throws IOException {
        Map<String, String> fdl = new HashMap<String, String>() {
            {
                put(
                        "HPLC",
                        IOUtils.toString(new InputStreamReader(this.getClass()
                                .getResourceAsStream("/HPLC.xml")))
                );
            }
        };

        this.server = ServerBuilder.forPort(devicePort)
                        .addService(new MeasurementImpl())
                        .build()
                        .start();

        bridge.registerDevice(DEVICE_NAME,
                new SiLAServiceDevice.DeviceInfo(
                        "HPLC Device",
                        "Virtualised HPLC device",
                        "SiLA Standard",
                        "www.sila-standard.org"),
                proxyPort, devicePort, fdl);

        Runtime.getRuntime().addShutdownHook(
                new Thread() {
                    @Override
                    public void run() {
                        System.err.println(
                                        "*** shutting down gRPC server since JVM is shutting down"
                        );
                        HPLCDevice.this.stop();
                        System.err.println("*** server shut down");
                    }
                }
        );
    }

    private void stop() {
        this.bridge.stop();
        this.server.shutdown();
    }

    public static void main(String[] args) throws IOException {
        final String usage = "Usage: (interface) (bridgePort) (devicePort)";

        String interfaceName = "local";
        int bridgePort = BRIDGE_PORT;
        int devicePort = DEVICE_PORT;

        if (args.length > 0) {
            interfaceName = args[0];
        }
        if (args.length > 1) {
            bridgePort = Integer.valueOf(args[1]);
        }
        if (args.length > 2) {
            devicePort = Integer.valueOf(args[2]);
        }
        if (args.length > 3) {
            throw new IllegalArgumentException(usage);
        }

        // Start Server
        final HPLCDevice server = new HPLCDevice(interfaceName);
        server.start(bridgePort, devicePort);
        blockUntilStop();
        server.stop();
        System.out.println("termination complete.");
    }

    static class MeasurementImpl extends HPLCGrpc.HPLCImplBase {
        private LinkedList<String> methodlist = new LinkedList<String>(Arrays.asList("injectSample", "getMethods", "addMethod", "deleteMethod", "updateMethod"));

        @Override
        public void injectSample(
                InjectSampleParameters req,
                StreamObserver<InjectSampleResponses> responseObserver
        ) {
            // @Note: Bridge already checks parameter presence, but shown here as an example
//            if (req.getPNameCase() == HPLCParameters.PNameCase.PNAME_NOT_SET) {
//                responseObserver.onError(generateValidationError(
//                                "Name",
//                                "Name parameter was not set."
//                        )
//                );
//                return;
//            }


            // @Note: Custom ValidationError example
//            String name = req.getName();
//            if ("error".equalsIgnoreCase(name)) {
//                responseObserver.onError(generateValidationError(
//                                "Name",
//                                "Name was called error therefore throw an error :)"
//                        )
//                );
//                return;
//            }

            String msg = "";
            try {
                msg = IOUtils.toString(new InputStreamReader(this.getClass()
                        .getResourceAsStream("/hplc.animl")));
            } catch(IOException e) {
                System.out.println("IOError");
                return;
            }
            InjectSampleResponses result = InjectSampleResponses.newBuilder().setSpectrum(msg).build();
            responseObserver.onNext(result);
            responseObserver.onCompleted();
            System.out.println("Request received on " + DEVICE_NAME + " " + msg);
        }

        @Override
        public void getMethods(
                GetMethodsParameters req,
                StreamObserver<GetMethodsResponses> responseObserver
        ) {
            String msg = String.join(";", this.methodlist);
            GetMethodsResponses result = GetMethodsResponses.newBuilder().setList(msg).build();
            responseObserver.onNext(result);
            responseObserver.onCompleted();
            System.out.println("Request received on " + DEVICE_NAME + " " + msg);
        }

        @Override
        public void addMethod(
                AddMethodParameters req,
                StreamObserver<AddMethodResponses> responseObserver
        ) {
            // @Note: Custom ValidationError example
            String name = req.getMethodName();
            if ("error".equalsIgnoreCase(name)) {
                responseObserver.onError(generateValidationError(
                                "MethodName",
                                "MethodName was called error therefore throw an error :)"
                        )
                );
                return;
            }

            this.methodlist.add(name);

            String msg = String.join(";", this.methodlist);
            AddMethodResponses result = AddMethodResponses.newBuilder().setList(msg).build();
            responseObserver.onNext(result);
            responseObserver.onCompleted();
            System.out.println("Request received on " + DEVICE_NAME + " " + msg);
        }

        @Override
        public void deleteMethod(
                DeleteMethodParameters req,
                StreamObserver<DeleteMethodResponses> responseObserver
        ) {
            // @Note: Custom ValidationError example
            String name = req.getMethodName();
            if ("error".equalsIgnoreCase(name)) {
                responseObserver.onError(generateValidationError(
                        "MethodName",
                        "MethodName was called error therefore throw an error :)"
                        )
                );
                return;
            }

            this.methodlist.remove(name);

            String msg = String.join(";", this.methodlist);
            DeleteMethodResponses result = DeleteMethodResponses.newBuilder().setList(msg).build();
            responseObserver.onNext(result);
            responseObserver.onCompleted();
            System.out.println("Request received on " + DEVICE_NAME + " " + msg);
        }

        @Override
        public void updateMethod(
                UpdateMethodParameters req,
                StreamObserver<UpdateMethodResponses> responseObserver
        ) {
            // @Note: Custom ValidationError example
            String name = req.getMethodName();
            if ("error".equalsIgnoreCase(name)) {
                responseObserver.onError(generateValidationError(
                        "MethodName",
                        "MethodName was called error therefore throw an error :)"
                        )
                );
                return;
            }

            // @Note: Custom ValidationError example
            String newname = req.getNewMethodName();
            if ("error".equalsIgnoreCase(newname)) {
                responseObserver.onError(generateValidationError(
                        "NewMethodName",
                        "NewMethodName was called error therefore throw an error :)"
                        )
                );
                return;
            }

            int pos = -1;
            for (String temp : this.methodlist) {
                pos++;
                if(temp.equals(name)) {
                    break;
                }
            }

            this.methodlist.set(pos, newname);

            String msg = String.join(";", this.methodlist);
            UpdateMethodResponses result = UpdateMethodResponses.newBuilder().setList(msg).build();
            responseObserver.onNext(result);
            responseObserver.onCompleted();
            System.out.println("Request received on " + DEVICE_NAME + " " + msg);
        }

    }
}
