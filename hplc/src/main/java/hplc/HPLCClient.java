package hplc;

import com.google.common.net.HostAndPort;
import com.google.protobuf.ProtocolStringList;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import sila_features.GetImplementedFeaturesParameters;
import sila_features.SiLAServiceGrpc;
import sila_library.discovery.SiLADeviceDiscovery;

import java.util.concurrent.TimeUnit;


/**
 * Client to retrieve Measurement from the {@link HPLCDevice}.
 */
public class HPLCClient {
    private ManagedChannel channel;
    private HPLCGrpc.HPLCBlockingStub blockingStub;

    private void buildChannel(String host, int port) {
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext(true)
                .build();
        this.channel = managedChannel;
        this.blockingStub = HPLCGrpc.newBlockingStub(managedChannel);
    }

    private void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    /**
     * Say hello to server.
     */
    private void measure(String sampleId, String methodname) {
        System.out.println("Will try to measure spectrum...");
        InjectSampleParameters.Builder parameter = InjectSampleParameters.newBuilder(); // .setName(name);
        InjectSampleResponses result;

        GetMethodsParameters.Builder parameter2 = GetMethodsParameters.newBuilder();
        GetMethodsResponses result_methods;

        // Comment out for trying:
        /*
         * System.out.println("Without parameter set:"); try {
         * blockingStub.sayHello(parameter.build()); throw new
         * RuntimeException("Should throw error without parameter!"); } catch
         * (StatusRuntimeException e) { System.out.println(e.getMessage()); }
         */

        try {
            result = blockingStub.injectSample(parameter.setSampleID(sampleId).setMethodName(methodname).build());
        } catch (StatusRuntimeException e) {
            System.out.println(e.getMessage());
            return;
        }

        try {
            result_methods = blockingStub.getMethods(parameter2.build());
            System.out.println("These methods are available: "+result_methods.getList());
        } catch (StatusRuntimeException e) {
            System.out.println(e.getMessage());
            return;
        }

        System.out.println("Result: " + result.getSpectrum());
    }

    public static void main(String[] args) throws Exception {
        HPLCClient client = new HPLCClient();
        SiLADeviceDiscovery deviceDiscovery = new SiLADeviceDiscovery();

        if (args.length == 0) {
            deviceDiscovery.Start("local");
        } else if (args.length == 1) {
            deviceDiscovery.Start(args[0]);
        } else {
            throw new IllegalArgumentException("Specify one interface or none");
        }

        // Blocking call to find device
        SiLADeviceDiscovery.blockUntilDeviceFound(HPLCDevice.DEVICE_NAME);
        HostAndPort channelInfo = SiLADeviceDiscovery.getChannelInfo(HPLCDevice.DEVICE_NAME);
        System.out.println("Found device");

        final String host = channelInfo.getHostText();
        final int port = channelInfo.getPort();

        ManagedChannel serviceChannel = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext(true)
                .build();
        SiLAServiceGrpc.SiLAServiceBlockingStub serviceStub = SiLAServiceGrpc
                .newBlockingStub(serviceChannel);

        System.out.println("Found Features:");
        ProtocolStringList featureIdentifierList = serviceStub
                .getImplementedFeatures(GetImplementedFeaturesParameters.newBuilder().build()).getImplementedFeaturesList();

        for (String featureIdentifier : featureIdentifierList) {
            System.out.println("\t" + featureIdentifier);
        }

        // Use the discovered channel
        client.buildChannel(host, port);
        try {
//            String user = "SiLA";
            client.measure("1", "test");
            // error
//            try {
//                client.greet("error");
//            } catch (Exception e) {
//                Status st = Status.fromThrowable(e);
//                System.out.println("client.greet(error) exception status:" + st);
//            }
        } finally {
            client.shutdown();
            deviceDiscovery.shutdown();
        }
    }
}
