# Virtual HPLC SiLA Device JAVA Implementation
The goal is to show how a SiLA device can be implemented in JAVA.

## Building and Running

To install the package, simply use maven:

    `mvn clean install`
    
Then run the main method of the `HPLCDevice` class.
