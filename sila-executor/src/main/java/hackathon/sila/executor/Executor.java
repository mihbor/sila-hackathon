package hackathon.sila.executor;

import akka.actor.ActorSystem;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.ContentTypes;
import akka.http.javadsl.model.HttpEntities;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Sink;
import akka.util.ByteString;
import barcodeprovider.BarcodeProviderGrpc;
import barcodeprovider.BarcodeProviderOuterClass.ReadCodeResponses;
import com.google.common.net.HostAndPort;
import com.google.protobuf.ProtocolStringList;
import dispensecontroller.DispenseControllerGrpc;
import hplc.HPLCGrpc;
import hplc.InjectSampleParameters;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import sila_features.GetImplementedFeaturesParameters;
import sila_features.SiLAServiceGrpc;
import sila_library.discovery.SiLADeviceDiscovery;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyException;
import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static barcodeprovider.BarcodeProviderGrpc.*;
import static barcodeprovider.BarcodeProviderOuterClass.*;
import static dispensecontroller.DispenseControllerGrpc.*;
import static dispensecontroller.DispenseControllerOuterClass.*;
import static hplc.HPLCGrpc.*;
import static java.time.temporal.ChronoUnit.SECONDS;

public class Executor {
    public static final String   BARCODE_DEVICE_NAME = "BarcodeReader";
    public static final String DISPENSER_DEVICE_NAME = "Multidrop";
    public static final String      HPLC_DEVICE_NAME = "HPLCDevice";
    private static String MERGER_HOST = "192.168.0.100";
    private static int MERGER_PORT = 8080;

    private ManagedChannel barcodeChannel;
    private ManagedChannel dispenserChannel;
    private ManagedChannel hplcChannel;
    private BarcodeProviderBlockingStub barcodeStub;
    private DispenseControllerBlockingStub dispenserStub;
    private HPLCBlockingStub hplcStub;

    private void initBarcode(String host, int port) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext(true)
                .build();
        this.barcodeChannel = channel;
        this.barcodeStub = BarcodeProviderGrpc.newBlockingStub(channel);
    }
    private void initDispenser(String host, int port) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext(true)
                .build();
        this.dispenserChannel = channel;
        this.dispenserStub = DispenseControllerGrpc.newBlockingStub(channel);
    }
    //High Performance Liquid Chromatography
    private void initHPLC(String host, int port) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port)
            .usePlaintext(true)
            .build();
        this.hplcChannel = channel;
        this.hplcStub = HPLCGrpc.newBlockingStub(channel);
    }

    private void shutdown() {
        barcodeChannel.shutdown();
        dispenserChannel.shutdown();
        hplcChannel.shutdown();
    }

    private String readCode() {
        System.out.println("Trying to read barcode ... ");
        ReadCodeResponses result = barcodeStub.readCode(ReadCodeParameters.getDefaultInstance());
        return result.getBarcode();
    }

    private String readCode(Duration timeout) {
        System.out.println("Trying to read barcode for up to " + timeout.getSeconds() + "s ... ");
        ReadUntilResponses result = barcodeStub.readUntil(
            ReadUntilParameters.newBuilder()
                .setTimeout(timeout.getSeconds())
                .build()
        );
        return result.getBarcode();
    }

    private void dispensePlate(long volume) {
        dispenserStub.dispensePlate(
            DispensePlateParameters.newBuilder()
                .setVolume(volume)
                .build()
        );
    }

    private void dispenseColumns(long volume, long start, long end) {
        dispenserStub.dispenseColumns(
            DispenseColumnsParameters.newBuilder()
                .setVolume(volume)
                .setColumnStart(start)
                .setColumnEnd(end)
                .build()
        );
    }

    private String injectSample(){
        String spectrum = hplcStub.injectSample(
            InjectSampleParameters.newBuilder()
                .setSampleID("s2")
                .setMethodName("mock")
                .build()
        ).getSpectrum();
        System.out.println("Spectrum: " + spectrum);
        return spectrum;
    }

    public static void main(String[] args) throws KeyException {
        String execId = UUID.randomUUID().toString();
        System.out.println("Starting execution id " + execId);
        Executor exec = new Executor();
        SiLADeviceDiscovery disco = new SiLADeviceDiscovery();

        if (args.length == 0) {
            disco.Start("local");
        } else if (args.length >= 1) {
            if(args.length >=2) {
                MERGER_HOST = args[1];
                if(args.length >=3) {
                    MERGER_PORT = Integer.parseInt(args[2]);
                }
            }
            disco.Start(args[0]);
        }
        // Blocking call to find device
        HostAndPort   barcodeInfo = discoverDevice(  BARCODE_DEVICE_NAME);
        HostAndPort dispenserInfo = discoverDevice(DISPENSER_DEVICE_NAME);
        HostAndPort      hplcInfo = discoverDevice(     HPLC_DEVICE_NAME);

        ManagedChannel serviceChannel = ManagedChannelBuilder
                .forAddress(barcodeInfo.getHostText(), barcodeInfo.getPort())
                .usePlaintext(true)
                .build();

        SiLAServiceGrpc.SiLAServiceBlockingStub serviceStub = SiLAServiceGrpc
                .newBlockingStub(serviceChannel);

        System.out.println("Found Features:");
        ProtocolStringList featureIdentifierList = serviceStub
                .getImplementedFeatures(GetImplementedFeaturesParameters.newBuilder().build())
                .getImplementedFeaturesList();

        for (String featureIdentifier : featureIdentifierList) {
            System.out.println("\t" + featureIdentifier);
        }
        // Use the discovered barcodeChannel
        exec.initBarcode  (  barcodeInfo.getHostText(),   barcodeInfo.getPort());
        exec.initDispenser(dispenserInfo.getHostText(), dispenserInfo.getPort());
        exec.initHPLC     (     hplcInfo.getHostText(),      hplcInfo.getPort());
        try {
            System.out.println("Scanning sample");
            String sampleId = exec.readCode(Duration.of(15, SECONDS));
            System.out.println("Sample ID: " + sampleId);
            merge(animlSample("barcode.animl", sampleId), execId)
                .thenCompose(r -> merge(animlSample("pipetting.animl", sampleId), execId))
                .thenCompose(r -> merge(exec.injectSample(), execId))
//                .thenCompose(r -> merge(animlSample("hplc.animl", sampleId), execId))
                .thenAccept(r -> uploadMergeResult(execId));
            exec.dispensePlate(10);
        } finally {
            exec.shutdown();
            disco.shutdown();
        }
    }

    private static HostAndPort discoverDevice(String deviceName) throws KeyException {
        // Blocking call to find device
        SiLADeviceDiscovery.blockUntilDeviceFound(deviceName);
        HostAndPort deviceInfo = SiLADeviceDiscovery.getChannelInfo(deviceName);
        System.out.println("Found " + deviceName);
        return deviceInfo;
    }

    private static String animlSample(String path, String sampleId) {
        try (Stream<String> lines = Files.lines(Paths.get("src/main/resources/" + path))) {
            String out = lines.map(line -> line.replace("SAMPLE_ID_HERE", sampleId))
                .collect(Collectors.joining());
            System.out.println(out);
            return out;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private static void uploadMergeResult(String execId) {
        ActorSystem system = ActorSystem.create();
        ActorMaterializer mat = ActorMaterializer.create(system);
        Http http = Http.get(system);
        http.singleRequest(
            HttpRequest.GET("http://" + MERGER_HOST + ":" + MERGER_PORT + "/execution/" + execId)
        ).thenApply(resp ->
            resp.entity().getDataBytes()
                .map(ByteString::utf8String)
                .runWith(Sink.foreach(System.out::println), mat)
        ).thenApply(r ->
            http.singleRequest(
                HttpRequest.POST("http://" + MERGER_HOST + ":" + MERGER_PORT + "/upload/" + execId)
            )
        );
    }
    private static CompletionStage<HttpResponse> merge(String animl, String execId) {
        return Http.get(ActorSystem.create()).singleRequest(
            HttpRequest.POST("http://" + MERGER_HOST + ":" + MERGER_PORT + "/execution/" + execId)
            .withEntity(
                HttpEntities.create(ContentTypes.TEXT_XML_UTF8, animl)
            )
        );
    }
}
