# SiLA Workflow Executor (SiLA Client) Implementation

## Building and Running

To install the package, simply use maven:

    `mvn clean install`
    
Then run the main method of the `Executor` class.
